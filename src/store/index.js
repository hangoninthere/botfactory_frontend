import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    botList: Array,
    botData: '',
    botDataCopy: '',
    changedElements: [],
    changed: false
  },
  mutations: {
    setBotList: (state, value) => {
      state.botList = value
    },
    newBot: (state, bot) => {
      state.botList.push(bot)
    },
    removeBot: (state, botIndex) => {
      state.botList.splice(botIndex, 1)
    },
    updateBot: (state, data) => {
      let index = data.index
      let newBot = data.bot
      state.botList.splice(index, 1, newBot)
    },
    isChanged: (state, value) => {
      let original = JSON.stringify(state.botData)
      let copied = JSON.stringify(state.botDataCopy)
      state.changed = original !== copied
      if (!state.changed) { state.changedElements = [] }
    },
    setBotData: (state, data) => {
      state.botData = data.original
      state.botDataCopy = data.copied
    },
    newScreen: (state, payload) => {
      let copiedData = JSON.parse(JSON.stringify(payload.screen))
      state.botData.screens.push(payload.screen)
      state.botDataCopy.screens.push(copiedData)
    },
    removeScreen: (state, screenIndex) => {
      state.botData.screens.splice(screenIndex, 1)
      state.botDataCopy.screens.splice(screenIndex, 1)
    },
    udateComponent: (state, data) => {
      let newComponent = JSON.parse(JSON.stringify(data.component))
      state.botData.screens[data.scI].screen_elements[data.elI] = newComponent
      // because of deep watching nested object dosn't supports by vue
      state.botData = JSON.parse(JSON.stringify(state.botData))
    },
    addComponent: (state, payload) => {
      state.botData.screens[payload.screenIndex].screen_elements.push(payload.component)
      state.botDataCopy.screens[payload.screenIndex].screen_elements.push(payload.component)
    }
  },
  getters: {
    getBotData: state => { return state.botData },
    getBotDataCopy: state => { return state.botDataCopy },
    changedElementsLenght: state => { return state.changedElements.length },
    cloneBotData: state => { return JSON.parse(JSON.stringify(state.botData)) }
  },
  actions: {
    getAllBots: ({ commit }) => {
      return new Promise((resolve, reject) => {
        Vue.axios.get('/bots').then(response => {
          commit('setBotList', response.data)
          resolve()
        }).catch(e => { reject(e) })
      })
    },
    createBot: ({ commit }) => {
      return new Promise((resolve, reject) => {
        Vue.axios.post('/bots').then(response => {
          commit('newBot', response.data)
          resolve()
        }).catch(e => { reject(e) })
      })
    },
    updateBot: ({ commit }, data) => {
      return new Promise((resolve, reject) => {
        let bot = data.bot
        Vue.axios.patch('/bots/' + bot.id, bot).then(response => {
          commit('updateBot', data)
          resolve()
        }).catch(e => { reject(e) })
      })
    },
    deleteBot: ({ commit }, data) => {
      return new Promise((resolve, reject) => {
        let index = data.index
        let bot = data.bot
        Vue.axios.delete('/bots/' + bot.id, { bot_id: bot.id }).then(response => {
          commit('removeBot', index)
          resolve()
        }).catch(e => { reject(e) })
      })
    },
    getBotData: ({ state }, botId) => {
      return Vue.axios.get('/bots/' + botId)
    },
    createScreen: ({ state }) => {
      return Vue.axios.post('/screens/', { bot_id: state.botData.bot.id })
    },
    updateScreens: ({ state, getters, dispatch }, url) => {
      return new Promise((resolve, reject) => {
        if (getters.changedElementsLenght > 0) {
          const url = '/bots/' + state.botData.bot.id + '/update_components'
          Vue.axios.patch(url, { changed_elements: state.changedElements }).then(response => {
            state.botDataCopy = getters.cloneBotData
            resolve(response.status)
          })
        } else {
          resolve('Not necessary')
        }
      })
    },
    updateComponentsProirity: ({ state }) => {
      return new Promise((resolve, reject) => {
        state.botData.screens.forEach((screen, i, arr) => {
          screen.screen_elements.forEach((elem, i, arr) => {
            if (elem.priority !== i + 1) {
              elem.priority = i + 1
              elem.screen_id = screen.id
            }
            if (elem.screen_id !== screen.id) {
              elem.screen_id = screen.id
            }
          })
        })
        resolve()
      })
    },
    findChangedComponents: ({ state }) => {
      return new Promise((resolve, reject) => {
        state.changedElements = []
        let counter = 0
        state.botData.screens.forEach((screen, screenI, arr) => {
          screen.screen_elements.forEach((elem, elemI, arr) => {
            let current = JSON.stringify(elem)
            let original = JSON.stringify(state.botDataCopy.screens[screenI].screen_elements[elemI])
            counter++
            if (current !== original) {
              state.changedElements.push(elem)
            }
          })
        })
        resolve(counter)
      })
    },
    letGetBotData: ({ dispatch, commit }, botId) => {
      return new Promise((resolve, reject) => {
        dispatch('getBotData', botId).then(response => {
          let original = response.data
          let copied = JSON.parse(JSON.stringify(response.data))
          commit('setBotData', { original: original, copied: copied })
          commit('isChanged')
          resolve()
        })
      })
    },
    letCreateScreen: ({ dispatch, commit }) => {
      return new Promise((resolve, reject) => {
        dispatch('createScreen').then(response => {
          commit('newScreen', response.data)
          resolve()
        })
      })
    },
    someComponentsUpdated: ({ dispatch, commit }) => {
      dispatch('updateComponentsProirity')
        .then(() => { dispatch('findChangedComponents') })
        .then(() => { commit('isChanged') })
    },
    letSaveScreens: ({ dispatch, state }) => {
      return new Promise((resolve, reject) => {
        dispatch('updateScreens').then(response => {
          dispatch('letGetBotData', state.botData.bot.id)
            .then(() => { resolve() })
        })
      })
    },
    letDeleteScreen: ({ dispatch, commit }, payload) => {
      return new Promise((resolve, reject) => {
        dispatch('updateScreens').then(response => {
          Vue.axios.delete(/screens/ + payload.screenId).then(response => {
            commit('removeScreen', payload.screenIndex)
            resolve()
          }).catch(e => reject(e))
        })
      })
    },
    letUpdateComponent: ({ state, commit, dispatch }, component) => {
      new Promise((resolve, reject) => {
        const BreakException = {}
        try {
          state.botData.screens.forEach((screen, screenIndex, arr) => {
            screen.screen_elements.forEach((elem, elemIndex, arr) => {
              if (elem.component_type === component.component_type && elem.id === component.id) {
                commit('udateComponent', { scI: screenIndex, elI: elemIndex, component: component })
                throw BreakException
              }
            })
          })
        } catch (e) {
          if (e !== BreakException) throw e
        }
        resolve()
      }).then(() => {
        dispatch('findChangedComponents')
      }).then(() => {
        commit('isChanged')
      })
    },
    letCreateComponent: ({ commit }, data) => {
      return new Promise((resolve, reject) => {
        Vue.axios.post('/screen_elements', data).then(response => {
          let payload = { screenIndex: data.screenIndex, component: response.data }
          commit('addComponent', payload)
          resolve()
        }).catch(e => { reject(e) })
      })
    }
  }
})
