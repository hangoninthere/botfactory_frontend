import Vue from 'vue'
import Router from 'vue-router'
import SignIn from '@/components/user/SignIn'
import SignUp from '@/components/user/SignUp'
import Root from '@/components/Root'
import Bot from '@/components/Bot'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'Root',
      path: '/',
      component: Root
    },
    {
      path: '/sign_in',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/sign_up',
      name: 'SignUp',
      component: SignUp
    },
    {
      name: 'Bot',
      path: '/bot/:id',
      component: Bot
    },
    {
      path: '*',
      component: Root
    }
  ]
})
