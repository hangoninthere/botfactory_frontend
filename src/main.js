// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { store } from './store'
import 'normalize.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

// components
import Draggable from 'vuedraggable'
import EditComponent from './components/bot/modals/ModalEditComponent'

// directives
import VueAutosize from 'vue-autosize'

Vue.use(VueAxios, axios)
Vue.use(VueAutosize)
Vue.component('draggable', Draggable)
Vue.component('edit-component', EditComponent)

Vue.config.productionTip = false

Vue.axios.interceptors.request.use(config => {
  config.baseURL = process.env.API_URL
  config.headers = {
    'access-token': localStorage.getItem('access-token'),
    'uid': localStorage.getItem('uid'),
    'client': localStorage.getItem('client')
  }
  return config
}, error => {
  return Promise.reject(error)
})

Vue.axios.interceptors.response.use(response => {
  let headers = response.headers
  if ('access-token' in headers && headers['access-token'] !== '') {
    localStorage.setItem('access-token', headers['access-token'])
    localStorage.setItem('uid', headers['uid'])
    localStorage.setItem('client', headers['client'])
  }
  return response
}, error => {
  if (error.config.url.includes('https://botapi.nfreeman.pw')) {
    if (error.response.status === 401) {
      router.push('/sign_in')
    } else {
      router.push('/')
    }
  }

  return Promise.reject(error)
})

const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
  $bus: {
    get: () => {
      return EventBus
    }
  }
})

/* eslint-disable no-new */
export default new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
